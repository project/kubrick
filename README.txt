Kubrick theme for phptemplate on Drupal

---------------------------------------

Last updated: 14 July 2007

This is a port of Kubrick theme to Drupal.

These elements of phptemplate can be displayed (all are optional)
- header
	- site name
	- site slogan
	- primary link
- content
	- mission statement
- right sidebar
	- search form (optional)
- site footer

These elements will not be rendered
- site logo image
- secondary link

http://drupal.org/project/kubrick

Authors:
- Gurpartap Singh (Current Project Maintainer for Drupal 4.7, 5)
- Chrisada Sookdhis (for Drupal 4.5, 4.6)

Contact: Use contact form at http://drupal.org/user/41470/contact
